package com.example.rashik.sharedpreferenceslogin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
EditText username,password;
Button button,button2;
TextView textView;
final public static String KEY="MYKEY";
SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(KEY,MODE_PRIVATE);
        username = (EditText)findViewById(R.id.user_name);
        textView = (TextView)findViewById(R.id.viewer);
        password = (EditText)findViewById(R.id.password);
        button = (Button)findViewById(R.id.login);
        button2 = (Button)findViewById(R.id.sharedview);
        SharedPreferences prefferd = getSharedPreferences(KEY,MODE_PRIVATE);
        String check_name = prefferd.getString("user_name","n/a");
        String check_spass = prefferd.getString("password","n/a");
        if(check_name.equals("admin") && check_spass.equals("admin"))
        {
            Intent intent = new Intent(getApplicationContext(),LoginSuccess.class);
            startActivity(intent);
            finish();
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = username.getText().toString();
                String pass = password.getText().toString();
                if(user.equals("admin") && pass.equals("admin"))
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user_name",user);
                    editor.putString("password",pass);
                    editor.commit();
                    Intent intent = new Intent(getApplicationContext(),LoginSuccess.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"INVALID USER NAME PASSWORD",Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user_name",user);
                    editor.putString("password",pass);
                    editor.commit();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefferd = getSharedPreferences(KEY, MODE_PRIVATE);
                String uname = prefferd.getString("user_name", "n/a");
                String spass = prefferd.getString("password","n/a");
                textView.setText(uname+"   "+spass);

            }
        });


    }
}
